package mst;

import java.util.Objects;

public class Position {
    private int key;
    private int value;

    Position() {
        setKey(0);
        setValue(0);
    }

    Position(int key, int value) {
        setKey(key);
        setValue(value);
    }

    public int getKey() {
        return key;
    }

    private void setKey(int key) {
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return key == position.key &&
                value == position.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

        @Override
        public String toString() {
                return "(" + key + ", " + value + ")";
        }
}
