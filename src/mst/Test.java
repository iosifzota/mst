package mst;

import containers.Int;

public class Test {
    private Test() {}

    public static void testGraph() {
        Graph<String, Integer, Integer> g;

        g = new Graph<>();
        if (!g.isEmpty()) {
            System.out.println("18");
            throw new AssertionError();
        }

        g.insertNode("kabal", 23);
        if (!g.hasNode("kabal")) {
            System.out.println("24");
            throw new AssertionError();
        }

        g.insertNode("trash", 63);
        if (!g.hasNode("trash")) {
            System.out.println("30");
            throw new AssertionError();
        }

        g.insertEdgeBi("kabal", "trash", 9);
        if (!g.hasEdge("kabal", "trash")) {
            System.out.println("36");
            throw new AssertionError();
        }

        g.insertEdgeBi("kabal", "tumbleweed", 10);
        if (g.hasEdge("kabal", "tumbleweed")) {
            System.out.println("42");
            throw new AssertionError();
        }

        g.removeEdgeOneWay("kabal", "trash");
        if (g.hasEdge("kabal", "trash")) {
            System.out.println("48");
            throw new AssertionError();
        }
        if (!g.hasEdge("trash", "kabal")) {
            System.out.println("49");
            throw new AssertionError();
        }

        g.removeNode("trash");
        if (g.hasNode("trash")) {
            System.out.println("58");
            throw new AssertionError();
        }
        if (g.hasEdge("trash", "kabal")) {
            System.out.println("62");
            throw new AssertionError();
        }

        g.insertNode("desert", 1);
        if (!g.hasNode("desert")) {
            System.out.println("68");
            throw new AssertionError();
        }

        g.insertEdgeOneWay("desert", "kabal", 29);
        if (!g.hasEdge("desert", "kabal")) {
            System.out.println("74");
            throw new AssertionError();
        }

        g.removeEdgeBi("desert", "kabal");
        if (g.hasEdge("desert", "kabal")) {
            System.out.println("80");
            throw new AssertionError();
        }

        g.insertEdgeBi("kabal", "desert", 21);
        if (!g.hasEdge("kabal", "desert") || !g.hasEdge("desert", "kabal"))  {
            System.out.println(86);
            throw new AssertionError();
        }
        if (!g.areNeighborsBi("kabal", "desert")) {
            System.out.println(91);
            throw new AssertionError();
        }

        if (g.isEmpty()) {
            System.out.println("92");
            throw new AssertionError();
        }

        g.insertNode("canada", 2);
        if (!g.hasNode("canada")) {
            System.out.println("98");
            throw new AssertionError();
        }

        g.insertEdgeBi("canada", "desert", 21);
        if (!g.hasEdge("canada", "desert") || !g.hasEdge("desert", "canada"))  {
            System.out.println(104);
            throw new AssertionError();
        }
        if (!g.areNeighborsBi("canada", "desert")) {
            System.out.println(91);
            throw new AssertionError();
        }

        for (String neighbor : g.neighborsOf("desert")) {
            System.out.println(neighbor);
        }

        System.out.println("test_graph(): DONE\n");
    }

    public static void other() {
        Graph<Int, String, Integer> graph = new Graph<>();

        graph.insertNode(Int.mk(4), "four");
        graph.insertNode(Int.mk(3), "three");

        String s = graph.nodeData(Int.mk(3));
        System.out.println(s);

        graph.insertEdgeBi(Int.mk(4), Int.mk(3), 7);
        assertThis(graph.hasEdge(Int.mk(4), Int.mk(3)) && graph.hasEdge(Int.mk(3), Int.mk(4)), "Should have edge");

    }

    public static void testUnionFind() {
        UnionFind uf = new UnionFind(10);
        uf.union(4, 3);
        assertThis(uf.connected(4, 3), "Must be connected");
        uf.union(3, 8);
        assertThis(uf.connected(3, 8), "Must be connected");
        assertThis(uf.connected(4, 8), "Must be connected");
        uf.union(6,5);
        assertThis(uf.connected(6, 5), "Must be connected");
        uf.union(9, 4);
        assertThis(uf.connected(9, 4), "Must be connected");
        assertThis(uf.connected(3, 9), "Must be connected");
        assertThis(uf.connected(8, 9), "Must be connected");
        uf.union(2, 1);
        assertThis(uf.connected(2, 1), "Must be connected");

        //
        assertThis(!uf.connected(0, 7), "Musn't be connected");

        uf.union(5, 0);
        assertThis(uf.connected(5, 0), "Must be connected");
        assertThis(uf.connected(6, 0), "Must be connected");
        uf.union(7, 2);
        assertThis(uf.connected(7, 2), "Must be connected");
        assertThis(uf.connected(7, 1), "Must be connected");
        uf.union(6, 1);
        assertThis(uf.connected(6, 1), "Must be connected");
        assertThis(uf.connected(6, 2), "Must be connected");
        assertThis(uf.connected(6, 7), "Must be connected");
        assertThis(uf.connected(5, 1), "Must be connected");
        assertThis(uf.connected(5, 2), "Must be connected");
        assertThis(uf.connected(5, 7), "Must be connected");
        assertThis(uf.connected(0, 1), "Must be connected");
        assertThis(uf.connected(0, 2), "Must be connected");
        assertThis(uf.connected(0, 7), "Must be connected now");
        uf.union(1, 0);
        assertThis(uf.connected(0, 1), "Must be connected same again");

    }

    public static void assertThis(Boolean b, String errMsg) throws AssertionError {
        if (!b) {
            throw new AssertionError(errMsg);
        }
    }
}
