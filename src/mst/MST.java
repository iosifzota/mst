package mst;

import javax.swing.JFrame;
import java.util.*;
import java.io.*;

import containers.Int;
import containers.Triplet;

/**
 * Hello world!
 *
 */
public class MST
{
        private static final String inputFilePath = "lab.in";

        public static void mst( String[] args ) {
//                Test.testGraph();
//                Test.other();
//                Test.testUnionFind();
                prim();
                kruskal();
        }

        public static void kruskal() {
                System.out.println("\n\nKruskal:");
                Graph<Int, String, Integer> graph;

                graph = readGraph("prim.txt");

                if (graph == null) {
                        throw new AssertionError("readGraph returned null.");
                }

                // print before
                System.out.print("Input:");
                graph.treeDoBi(
                        (root) -> {
                                System.out.print("\n" + root);
                                System.out.print("  ");
                        },
                        (nonRoot) -> {
                                System.out.print("->  " + nonRoot);
                                System.out.print("  ");
                        });

                // sort edges
                PriorityQueue<Triplet<Int, Int, Integer>> sortedEdges = graph.getSortedEdges(0);

                // BEGIN
                Graph<Int, Integer, Integer> mst = new Graph<>(graph.getCap());
                UnionFind comp = new UnionFind(graph.getCap());

                int n = graph.getCap() - 1; // cap == n + 1
                int m = n - 1;

                System.out.println("Begin:");
                int edgeCounter = m;
                // itr: O(n)  // m = n - 1
                while(!sortedEdges.isEmpty() && edgeCounter > 0) {
                        Triplet<Int, Int, Integer> triplet = sortedEdges.poll();

                        // get begin, end, cost
                        Int begin = triplet.getFirst();
                        Int end = triplet.getSecond();
                        int cost = triplet.getThird();

                        // ignore already connected components
                        // O(lg n)
                        if (comp.connected(begin.get(), end.get())) {
                                System.out.println("Ignore: " + begin + ", " + end);
                                continue; // begin # N, end # N_barat
                        }

                        // O(lg n)
                        comp.union(end.get(), begin.get());

                        // insert nodes and edge
                        mst.insertNode(begin, begin.get());
                        mst.insertNode(end, end.get());
                        mst.insertEdgeBi(begin, end, cost);

                        edgeCounter -= 1; // added edge
                }

                if (edgeCounter != 0) {
                        throw new AssertionError("The graph mst is not a tree: " + edgeCounter);
                }

                // print mst
                System.out.print("\nArbore partial de cost minim:");
                mst.treeDoBi(
                        (root) -> {
                                System.out.print("\n" + root);
                                System.out.print("  ");
                        },
                        (nonRoot) -> {
                                System.out.print("->  " + nonRoot);
                                System.out.print("  ");
                        });
                System.out.println('\n');
        }

        public static void prim() {
                Graph<Int, String, Integer> graph;

                graph = readGraph("prim.txt");

                if (graph == null) {
                        throw new AssertionError("readGraph returned null.");
                }

                // print before
                System.out.println("Prim:");
                System.out.print("Input:");
                graph.treeDoBi(
                        (root) -> {
                                System.out.print("\n" + root);
                                System.out.print("  ");
                        },
                        (nonRoot) -> {
                                System.out.print("->  " + nonRoot);
                                System.out.print("  ");
                        });

                // get minimum spanning tree
                Graph<Int, String, Integer>  mst = graph.primAlg(0);

                System.out.print("Arbore partial de cost minim:");
                mst.treeDoBi(
                        (root) -> {
                                System.out.print("\n" + root);
                                System.out.print("  ");
                        },
                        (nonRoot) -> {
                                System.out.print("->  " + nonRoot);
                                System.out.print("  ");
                        });
        }

        public static Graph<Int, String, Integer> readGraph(String inputFilePath) {
                Graph<Int, String, Integer> graph = null;
                File inputFile = new File(inputFilePath);

                try (Scanner in = new Scanner(new BufferedReader( new FileReader(inputFile) ) )) {

                        int n = 0, counter = 0;

                        // read number of nodes
                        if (in.hasNext() && in.hasNextInt()) {
                                n = counter = in.nextInt();
                        }

                        graph = new Graph<>(n + 1);

                        // keys store
                        Int[] keys = new Int[n + 1];

                        // read the nodes
                        while (in.hasNext() && in.hasNextInt() && counter-- > 0) {

                                // next node
                                int key = in.nextInt();

                                // valid key
                                if (key < 0 || key >= keys.length) {
                                        throw new AssertionError("Invalid node key: key # [0, " + n + "]. Read: " + key);
                                }

                                // store
                                keys[key] = Int.mk(key);

                                // check for node data
                                if (!in.hasNext() && !in.hasNext("\\w")) {
                                        counter = n;
                                        break;
                                }

                                // read node data
                                String data = in.next();

                                // inset node
                                graph.insertNode(keys[key], data);
                        }

                        // all input read?
                        if (counter != -1) {
                                throw new AssertionError("Input must have at least " + n + " number of nodes");
                        }

                        // read edges
                        while(in.hasNext() && in.hasNextInt()) {
                                // read begin
                                int begin = in.nextInt();

                                // check for end
                                if (!in.hasNextInt()) {
                                        throw new AssertionError("Missing edge end.");
                                }
                                // read end
                                int end = in.nextInt();

                                // check begin, end
                                if (begin < 0 || end < 0 ||
                                        begin >= keys.length || end >= keys.length)
                                {
                                        throw new AssertionError("Invalid node key: key # [0, " + keys.length + ")");
                                }

                                // check nodes
                                if (!graph.hasNode(keys[begin]) || !graph.hasNode(keys[end])) {
                                        throw new AssertionError("Edge node/nodes were not insert in the graph.");
                                }

                                // check for edge cost
                                if (!in.hasNextInt()) {
                                        throw new AssertionError("Missing edge cost.");
                                }
                                // read edge cost
                                int edgeCost = in.nextInt();

                                // inset undirected edge
                                graph.insertEdgeBi(keys[begin], keys[end], edgeCost);
                        }
                } catch (java.io.FileNotFoundException e) {
                        System.out.println("File " + inputFilePath + " not found.");
                }
                return graph;
        }





























































        private static <NodeData, EdgeData extends Comparable<EdgeData>> void readStringGraph(String inputFileName, Graph<String, NodeData, EdgeData> g, NodeData defNodeVal, EdgeData defEdgeVal) {
                File inputFile = new File(inputFileName);
                /*
                 * PrintWriter.output = new PrintWriter(file);
                 * output.println("blabla");
                 * output.close();
                 */

                try {
                        Scanner in = new Scanner(inputFile);

                        while (in.hasNext("\\w*")) {
                                String begin = in.next("\\w*");
                                if (in.hasNext("\\w*")) {
                                        String end = in.next("\\w*");
                                        g.insertNode(begin, defNodeVal);
                                        g.insertNode(end, defNodeVal);
                                        g.insertEdgeOneWay(begin, end, defEdgeVal);
                                }
                        }
                        in.close();
                } catch (FileNotFoundException e) {
                        System.out.println("File not found! " + e);
                        throw new AssertionError();
                }
        }

        private static void initUI(Labyrinth labyrinth) {
                Test.assertThis(labyrinth != null, "initUI: Null input.");

                JFrame f = new JFrame("Labyrinth");
                // Close on exit
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                // Add canvas
                f.add(new Canvas(labyrinth));
                // Frame width & height
                f.setSize(500, 500);
                // Show frame
                f.setVisible(true);
        }


        // teme: 1 & 2
        public static void other() {
                Test.testGraph();
                Labyrinth labyrinth = new Labyrinth(inputFilePath);

                // labyrinth.printMatrix();
                // labyrinth.solve();
                // labyrinth.printMatrix();
                /**
                 * Pornesc firul de executie grafic fie prin implementarea interfetei Runnable,
                 * fie printr-un obiect al clasei Thread.
                 */
                //SwingUtilities.invokeLater(() -> initUI(labyrinth));

                System.out.println("--------------------#");

                Graph<String, Integer, Integer> s = new Graph<>();
                s.insertNode("one", 1);
                s.insertNode("two", 2);
                s.insertNode("three", 3);
                s.insertNode("four", 4);
                s.insertNode("five", 5);
                s.insertNode("six", 6);
                s.insertNode("seven", 7);

                s.insertEdgeOneWay("one", "two", 1);
                s.insertEdgeOneWay("one", "four", 2);
                s.insertEdgeOneWay("two", "four", 3);
                s.insertEdgeOneWay("four", "three", 4);
                s.insertEdgeOneWay("four", "five", 5);
                s.insertEdgeOneWay("five", "seven", 6);
                s.insertEdgeOneWay("seven", "six", 7);

                List<String> sorted = s.topologicSort("one");
                System.out.println("Sorted: ");
                for (String key : sorted) {
                        System.out.print(key + " ");
                }
                System.out.println("");

                System.out.println("--------------------#");

                s.transformToUndirected(0);
                s.insertNode("solo", 10);
                List<List<String>> allPacks = s.connectedComponents();
                int nthPack = 0;
                for (List<String> pack : allPacks) {
                        System.out.println("Pack " + nthPack++ + ":");
                        for (String key : pack) {
                                System.out.print(key + " ");
                        }
                        System.out.println("");
                }

                System.out.println("\n--------------------#");

                Graph<String, Integer, Integer> ex = new Graph<>();

                // readStringGraph("strong.in", ex, 0, 0); TODO(fix)

                ex.insertNode("one", 1);
                ex.insertNode("two", 2);
                ex.insertNode("three", 3);
                ex.insertNode("four", 4);
                ex.insertNode("five", 5);
                ex.insertNode("six", 6);
                ex.insertNode("seven", 7);
                ex.insertNode("eight", 8);

                ex.insertEdgeOneWay("one", "two", 0);
                ex.insertEdgeOneWay("two", "six", 0);
                ex.insertEdgeOneWay("two", "three", 0);
                ex.insertEdgeOneWay("two", "five", 0);
                ex.insertEdgeBi("six", "seven", 0);
                ex.insertEdgeOneWay("seven", "eight", 0);
                ex.insertEdgeOneWay("three", "seven", 0);
                ex.insertEdgeBi("three", "four", 0);
                ex.insertEdgeOneWay("four", "eight", 0);
                ex.insertEdgeOneWay("five", "six", 0);
                ex.insertEdgeOneWay("five", "one", 0);

                final Graph<String, List<String>, Integer> packsGraph = ex.strongConnectedComponents(0);

                packsGraph.treeDo(
                        (root) -> {
                                System.out.print("\nComponent " + root + ": ");
                                for (String elem : packsGraph.nodeData(root)) {
                                        System.out.print(elem + " ");
                                }
                                System.out.print("  ");
                        },
                        (nonRoot) -> {
                                System.out.print("-> Component " + nonRoot + ": ");
                                for (String elem : packsGraph.nodeData(nonRoot)) {
                                        System.out.print(elem + " ");
                                }
                                System.out.print("  ");
                        });


        }
}
