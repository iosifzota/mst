package containers;

public class Int {
    private int data;

    public Int(int value) {
        set(value);
    }

    public Int(String s) throws NumberFormatException {
        set(s);
    }

    public static Int mk(int integer) {
        return new Int(integer);
    }

    public static Int mk(String s) {
        return new Int(s);
    }

    public Integer get() {
        return data;
    }

    public void set(int integer) {
        this.data = integer;
    }

    public void set(String s) throws NumberFormatException {
        this.data = Integer.valueOf(s);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Int anInt = (Int) o;
        return data == anInt.data;
    }

    /**
     * @return f(x) = x
     */
    @Override
    public int hashCode() {
        return data;
    }

    @Override
    public String toString() {
        return Integer.toString(data);
    }
}
