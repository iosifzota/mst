package containers;

import java.util.HashMap;
import java.util.Set;

public class MyMap<K, V> implements IMap<K, V> {
    public final HashMap<K, V> container = new HashMap<>();

    @Override
    public boolean containsKey(K key) {
        return container.containsKey(key);
    }

    @Override
    public V get(K key) {
        return container.get(key);
    }

    @Override
    public boolean isEmpty() {
        return container.isEmpty();
    }

    @Override
    public Set<K> keySet() {
        return container.keySet();
    }

    @Override
    public V put(K key, V value) {
        return container.put(key, value);
    }

    @Override
    public V remove(K key) {
        return container.remove(key);
    }

    @Override
    public int size() {
        return container.size();
    }
}
