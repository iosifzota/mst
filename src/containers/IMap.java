package containers;

import java.util.Set;

public interface IMap<K, V> {
    public boolean containsKey(K key);
    public V get(K key);
    public boolean isEmpty();
    public Set<K> keySet();
    public V put(K key, V value);
    public V remove(K key);
    public int size();
}
