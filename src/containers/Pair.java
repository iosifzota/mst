package containers;

import java.util.Objects;

public class Pair<T, U extends Comparable<U> > implements Comparable<Pair<T, U>> {
    private T first;
    private U second;

    public Pair(T first, U second) {
        if (null == first || null == second) {
            throw new AssertionError("No null data");
        }

        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public U getSecond() {
        return second;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public void setSecond(U second) {
        this.second = second;
    }

    public boolean same(Pair<T, U> other) {
        if (null == other) return false;
        return this == other || first.equals(other.first) && second.equals(other.second);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return second.equals(pair.second);
    }

    @Override
    public int compareTo(Pair<T, U> o) {
        return this.second.compareTo(o.second);
    }

    @Override
    public String toString() {
        return "(" + first + ", " + second + ")";
    }
}