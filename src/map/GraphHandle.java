package map;

import containers.Int;
import containers.Pair;
import mst.Graph;

import mst.Test;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.function.BiFunction;

public class GraphHandle extends DefaultHandler {
    private int nodesCounter = 0;

    private Graph<Int, Pair<Integer, Integer>, Integer> graph = new Graph<>();
    private Pair<Integer, Integer> minPos = new Pair<>(0, 0);
    private Pair<Integer, Integer> maxPos = new Pair<>(0, 0);
    private boolean parsed = false;

    public Graph<Int, Pair<Integer, Integer>, Integer> parse(String filePath) {
        if (parsed) {
            return graph;
        }

        try(BufferedInputStream input = new BufferedInputStream(new FileInputStream(filePath))) {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(input, this);
            System.out.println("Nr. of nodes: " + this.getNodesCounter());
        } catch (Exception e) {
            // reset state
            graph = new Graph<>();
            minPos = new Pair<>(0, 0);
            maxPos = new Pair<>(0, 0);
            e.printStackTrace();
        }

        parsed = true;
        return graph;
    }

    private void updateCmp(Pair<Integer, Integer> candidate,
                           Pair<Integer, Integer> current,
                           BiFunction<Integer, Integer, Boolean> cmp) {

        if (cmp.apply(candidate.getFirst(), current.getFirst())) {
            current.setFirst(candidate.getFirst());
        }
        if (cmp.apply(candidate.getSecond(), current.getSecond())) {
            current.setSecond(candidate.getSecond());
        }
    }

    private void updateCorners(Pair<Integer, Integer> candidate) {

        if (minPos.getFirst() == 0) {
            minPos.setFirst(candidate.getFirst());
            minPos.setSecond(candidate.getSecond());
        }
        updateCmp(candidate, minPos, (cand, current) -> (cand < current));
        updateCmp(candidate, maxPos, (cand, current) -> (cand > current));
    }

    @Override
    public void startElement(
            String uri,
            String localName,
            String qName,
            Attributes attributes)
    {
        if (qName.equalsIgnoreCase("node")) {
            final int id = Integer.parseInt(attributes.getValue(0));

            // reversed input long <-> lat
            final int lat = Integer.parseInt(attributes.getValue("longitude"));
            final int lng = Integer.parseInt(attributes.getValue("latitude"));

            final Pair<Integer, Integer> coord = new Pair<>(lng, lat);
            graph.insertNode(Int.mk(id), coord);
            Test.assertThis(id == getNodesCounter(), "Missing node.");
            countNode();

            updateCorners(coord);

        } else if (qName.equalsIgnoreCase("arc")) {
            final int begin = Integer.parseInt(attributes.getValue(0));
            final int end = Integer.parseInt(attributes.getValue(1));
            final int len = Integer.parseInt(attributes.getValue(2));

            Test.assertThis(graph.hasNode(Int.mk(begin)), "");
            Test.assertThis(graph.hasNode(Int.mk(end)), "");
            graph.insertEdgeBi(Int.mk(begin), Int.mk(end), len);
            Test.assertThis(graph.hasEdge(Int.mk(begin), Int.mk(end)), "Edge insertion failed.");
            Test.assertThis(graph.hasEdge(Int.mk(end), Int.mk(begin)), "Edge insertion failed.");
        }
    }

    @Override
    public void endElement(
            String uri,
            String localName,
            String qName)
    {

    }

    @Override
    public void characters(
            char ch[],
            int start,
            int length)
    {

    }

    public Graph<Int, Pair<Integer, Integer>, Integer> getGraph() {
        return graph;
    }

    public Pair<Integer, Integer> getMinPos() {
        return minPos;
    }

    public Pair<Integer, Integer> getMaxPos() {
        return maxPos;
    }

    public int getNodesCounter() {
        return nodesCounter;
    }

    private void countNode() {
        ++nodesCounter;
    }
}
