package map;

import containers.Int;
import containers.Pair;
import mst.Graph;
import mst.Test;

import javax.swing.*;

public class Map {

    public static void main(String[] args) {
        GraphHandle graphHandle = new GraphHandle();
        Graph<Int, Pair<Integer, Integer>, Integer> graph;

        graph = graphHandle.parse("map2.xml");
        Test.assertThis(graph.nodesCardinal() == graphHandle.getNodesCounter(), "Not cool");

        System.out.println("Max: " + graphHandle.getMaxPos());
        System.out.println("Min: " + graphHandle.getMinPos());

        SwingUtilities.invokeLater(() -> initUI(graph, graphHandle));
    }

    private static void initUI(Graph<Int, Pair<Integer, Integer>, Integer> graph, GraphHandle graphHandle) {
        final int width = 1000;
        final int height = 1000;

        Canvas canvas = new Canvas(graph, graphHandle, width, height);

        JFrame f = new JFrame("Luxembourg");
        // Close on exit
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Add canvas
        f.add(canvas);
        // Frame width & height
        f.setSize(width, height);
        // Show frame
        f.setVisible(true);

        System.out.println(canvas.scale);
    }
}
