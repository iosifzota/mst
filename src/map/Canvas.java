package map;

import containers.Int;
import containers.Pair;
import mst.Graph;
import mst.Test;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class Canvas extends JPanel {

    private final Graph<Int, Pair<Integer, Integer>, Integer> graph;
    private final Pair<Integer, Integer> minPos;
    private final Pair<Integer, Integer> maxPos;
    private final int mapWidth;
    private final int mapHeight;
    private final int padding = 10;
    public final float scale;

    private Int first = null;
    private Int second = null;

    private HashMap<Int, Int> visited = null;


    public Canvas(Graph<Int, Pair<Integer, Integer>, Integer> graph, GraphHandle graphHandle, final int windowWidth, final int windowHeight) {
        Test.assertThis(null != graph, "Null input.");
        Test.assertThis(null != graphHandle, "Null input.");

        this.graph = graph;
        this.minPos = graphHandle.getMinPos();
        this.maxPos = graphHandle.getMaxPos();

        final float dx = maxPos.getFirst() - minPos.getFirst();
        final float dy = maxPos.getSecond() - minPos.getSecond();

        this.mapWidth = windowWidth - 2 * padding;
        this.mapHeight = windowHeight - 2 * padding;
        this.scale = min(mapWidth / dx, mapHeight / dy);

        // display
        scaleGraph();

        // input handle
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Pair<Integer, Integer> coords = new Pair<>(e.getX() - padding, e.getY() - padding);
                System.out.println(coords);

                Int minId = findClosest(coords);
                if (null != minId) {
                    Pair<Integer, Integer> closest = graph.nodeData(minId);
                    System.out.println("Closest: " + closest);

                    if (null == first || null != second) {
                        first = minId;

                        if (null != second) {
                            second = null;
                        }
                    } else {
                        second = minId;
                        search();
                    }
                } else {
                    System.out.println("Niet");
                }
            }
        });
    }

    public void search() {
        Test.assertThis(null != first, "");
        Test.assertThis(null != second, "");

        if (first.equals(second)) {
            System.out.println("Same point with both clicks. Try again!");
            second = (first = null); // reset
            return;
        }

        this.visited = graph.Dijkstra(first, second, 0,
                (lhs, rhs) -> (lhs + rhs));

        if (null == this.visited) {
            System.out.println("Not found end: " + second);
        }
        repaint();
    }

    public void drawPath(Int end, Graphics g) {
        g.setColor(Color.RED);

        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(3));

        while (!end.equals(visited.get(end))) {
            Test.assertThis(graph.hasNode(end), "missing end");
            Test.assertThis(visited.containsKey(end), "not in the search path");
            Test.assertThis(graph.hasNode(visited.get(end)), "missing other");

            Pair<Integer, Integer> beginCoord = graph.nodeData(visited.get(end));
            Pair<Integer, Integer> endCoord = graph.nodeData(end);

            drawEdge(beginCoord, endCoord, g2);
            end = visited.get(end);
        }
        g2.setStroke(new BasicStroke(1));
        g2.setColor(Color.BLACK);
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.BLACK);

        for (Map.Entry<Int, HashMap<Int, Integer>> beginTo : graph.getEdgesSet()) {
            Pair<Integer, Integer> beginCoord = graph.nodeData(beginTo.getKey());

            for (Int endKey : beginTo.getValue().keySet()) {
                Pair<Integer, Integer> endCoord = graph.nodeData(endKey);
                drawEdge(beginCoord, endCoord, g);
            }
        }

        if (null != visited && null != second && null != first) {
            drawPath(second, g);
        }
    }

    protected void drawEdge(Pair<Integer, Integer> beginCoord, Pair<Integer, Integer> endCoord, Graphics g) {
        // checks
        Test.assertThis(beginCoord.getFirst() <= mapWidth, beginCoord.getFirst() + " <=W" + mapWidth);
        Test.assertThis(beginCoord.getSecond() <= mapHeight, beginCoord.getSecond() + " <=H " + mapHeight);
        Test.assertThis(endCoord.getFirst() <= mapWidth, endCoord.getFirst() + " <=W " + mapWidth);
        Test.assertThis(endCoord.getSecond() <= mapHeight, endCoord.getSecond() + " <=H " + mapHeight);

        g.drawLine(beginCoord.getFirst() + padding, beginCoord.getSecond() + padding,
                endCoord.getFirst() + padding, endCoord.getSecond() + padding);
    }

    public Pair<Integer, Integer> scaleCoord(Pair<Integer, Integer> coord) {
        coord.setFirst((int)((float)(coord.getFirst() - minPos.getFirst()) * scale));
        coord.setSecond((int)((float)(maxPos.getSecond() - coord.getSecond()) * scale));
        return coord;
    }

    private void scaleGraph() {
        for (Map.Entry<Int, Pair<Integer, Integer>> node : graph.getNodes()) {
            scaleCoord(node.getValue());
        }
    }

    private Int findClosest(Pair<Integer, Integer> clickCoords) {
        int minDist = 0;
        Int minId = null;
        for (Map.Entry<Int, Pair<Integer, Integer>> node : graph.getNodes()) {
            final int dist = distance(clickCoords, node.getValue());

            if (dist < minDist || null == minId) {
                minDist = dist;
                minId = node.getKey();
            }
        }
        return minId;
    }

    private static int distance(Pair<Integer, Integer> one, Pair<Integer, Integer> other) {
        final int dx = abs(one.getFirst() - other.getFirst());
        final int dy = abs(one.getSecond() - other.getSecond());

        return dx*dx + dy*dy;
    }
}
