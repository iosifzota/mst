package maxflux;

import containers.Int;
import containers.Pair;
import mst.Graph;

public class MaxFlux {

    static String inputFilePath = "maxflux.in";

    public static void main(String[] args) {
        Graph<Int, Integer, Pair<Integer, Integer>> graph;
        Graph<Int, Integer, Integer> residualGraph;

        // input
        graph = Util.readGraph(inputFilePath);
        graph.treeDo(
                (nodeKey) -> System.out.print(nodeKey + " "),
                (nodeKey) -> System.out.print(nodeKey + " ")
        );
        graph.forEachEdge((begin, end, edgeData) -> System.out.println(begin + " - " + end + " " + edgeData));

        // get residual for maxFlux
        residualGraph = graph.maxFlux(Util::getResidualGraph, Int.mk(1), Int.mk(4));
        // get updated graph
        graph = Util.updateUsingResidualGraph(graph, residualGraph);

        // output
        System.out.println("End:");
        graph.forEachEdge((begin, end, edgeData) -> System.out.println(begin + " - " + end + " " + edgeData));
    }
}
