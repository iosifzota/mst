package maxflux;

import containers.Int;
import containers.Pair;
import mst.Graph;
import mst.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;

public abstract class Util {
    public static Graph<Int, Integer, Pair<Integer, Integer>> readGraph(String inputFilePath) {
        Graph<Int, Integer, Pair<Integer, Integer>> graph = new Graph<>();

        try (Scanner in = new Scanner( new BufferedReader( new FileReader(inputFilePath)))) {
            String line;
            while (in.hasNextLine() && (line = in.nextLine()) != null) {
                String[] args = line.trim().split(",");
                Test.assertThis(args.length == 3, "Should be 3 args. Found " + args.length);

                // get nodes
                Int begin, end;
                begin = Int.mk(args[0].trim());
                end   = Int.mk(args[1].trim());
                // insert
                graph.insertNode(begin, 0);
                graph.insertNode(end, 0);

                // get edge data
                args = args[2].split(":");
                Test.assertThis(args.length == 2 || args.length == 1, "Should be 1 or 2 args. Found " + args.length);
                int flux, cap;

                // arrange pair members
                flux = Integer.parseInt(args[0].trim());
                if (args.length == 2) {
                    cap = Integer.parseInt(args[1].trim());
                } else {
                    cap = flux;
                    flux = 0;
                }
                Pair<Integer, Integer> edgeData = new Pair<>(flux, cap);
                // insert
                graph.insertEdgeOneWay(begin, end, edgeData);
                // checks
                Test.assertThis(graph.hasEdge(begin, end), "Input read was erroneous.");
                graph.edgeData(begin, end).same(edgeData);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File " + inputFilePath + " not found.");
        }

        return graph;
    }

    public static Graph<Int, Integer, Integer> getResidualGraph(Graph<Int, Integer, Pair<Integer, Integer>> graph) {
        Graph<Int, Integer, Integer> residualGraph = new Graph<>();

        graph.forEachEdge((begin, end, directEdgeData) -> {
            if (residualGraph.hasEdge(begin, end)) {
                System.out.println("Skipping edge.");
                System.exit(-1);
            }
            int directCap, directFlux;

            // direct edge data
            directCap = directEdgeData.getSecond();
            directFlux = directEdgeData.getFirst();

            // inverse edge data
            Test.assertThis(graph.edgeData(end, begin) == null, "Cannot have both direct and inverse edge.");

            // calculate residuals
            int directResidualData, inverseResidualData;
            directResidualData  = directCap - directFlux;
            inverseResidualData = directFlux;

            // insert nodes and edge
            residualGraph.insertNode(begin, 0);
            residualGraph.insertNode(end, 0);
            if (0 != directResidualData) {
                residualGraph.insertEdgeOneWay(begin, end, directResidualData);
            }
            if (0 != inverseResidualData) {
                residualGraph.insertEdgeOneWay(end, begin, inverseResidualData);
            }
        });

        return residualGraph;
    }

    public static Graph<Int, Integer, Pair<Integer, Integer>> updateUsingResidualGraph(
            Graph<Int, Integer, Pair<Integer, Integer>> graph,
            Graph<Int, Integer, Integer> residualGraph) {

        Graph<Int, Integer, Pair<Integer, Integer>> updatedGraph = new Graph<>();

        graph.forEachEdge((begin, end, edgeData) -> {
            int cap, r, flux;
            cap = edgeData.getSecond();
            r = (residualGraph.hasEdge(begin, end)) ? residualGraph.edgeData(begin, end) : 0;
            flux = cap - r;

            updatedGraph.insertNode(begin, 0);
            updatedGraph.insertNode(end, 0);
            if (flux > 0) {
                updatedGraph.insertEdgeOneWay(begin, end, new Pair<>(flux, cap));
            } else if (0 != flux) {
                updatedGraph.insertEdgeOneWay(end, begin, new Pair<>(-flux, cap));
            }
        });

        return updatedGraph;
    }

    public static <T extends Iterable<?>> void printIterable(T iterable) {
        Iterator<?> itr = iterable.iterator();
        itr.forEachRemaining((e) -> System.out.print(e + " "));
    }
}
