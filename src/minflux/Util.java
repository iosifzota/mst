package minflux;

import containers.Int;
import containers.Pair;
import containers.Triplet;
import mst.Graph;
import mst.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Util {
    public static Graph<Int, Integer, Triplet<Integer, Integer, Integer>> readGraph(String inputFilePath) {
        Graph<Int, Integer, Triplet<Integer, Integer, Integer>> graph = new Graph<>();

        try (Scanner in = new Scanner( new BufferedReader( new FileReader(inputFilePath)))) {
            String line;
            while (in.hasNextLine() && (line = in.nextLine()) != null) {
                String[] args = line.trim().split(",");
                Test.assertThis(args.length == 3, "Should be 3 args. Found " + args.length);

                // get nodes
                Int begin, end;
                begin = Int.mk(args[0].trim());
                end   = Int.mk(args[1].trim());
                // insert
                graph.insertNode(begin, 0);
                graph.insertNode(end, 0);

                // get edge data
                args = args[2].split(":");
                Test.assertThis(args.length == 3 || args.length == 2, "Should be 1 or 2 args. Found " + args.length);
                int flux, cap, cost;

                // arrange pair members
                flux = Integer.parseInt(args[0].trim());
                cap = Integer.parseInt(args[1].trim());
                if (args.length == 3) {
                    cost = Integer.parseInt(args[2].trim());
                } else {
                    cost = cap;
                    cap = flux;
                    flux = 0;
                }
                Triplet<Integer, Integer, Integer> edgeData = new Triplet<>(flux, cap, cost);
                // insert
                graph.insertEdgeOneWay(begin, end, edgeData);
                // checks
                Test.assertThis(graph.hasEdge(begin, end), "Input read was erroneous.");
                graph.edgeData(begin, end).same(edgeData);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File " + inputFilePath + " not found.");
        }

        return graph;
    }

    public static Graph<Int, Integer, Pair<Integer, Integer>> getResidualGraph(Graph<Int, Integer, Triplet<Integer, Integer, Integer>> graph) {
        Graph<Int, Integer, Pair<Integer, Integer>> residualGraph = new Graph<>();

        graph.forEachEdge((begin, end, edgeData) -> {
            if (residualGraph.hasEdge(begin, end)) {
                System.out.println("Skipping edge.");
                System.exit(-1);
            }
            int directFlux, directCap, directCost;

            // direct edge data
            directFlux = edgeData.getFirst();
            directCap = edgeData.getSecond();
            directCost = edgeData.getThird();

            // inverse edge data
            Test.assertThis(graph.edgeData(end, begin) == null, "Cannot have both direct and inverse edge.");

            // calculate residuals
            int directResidual, inverseResidual;
            directResidual = directCap - directFlux;
            inverseResidual = directFlux;

            // insert nodes and edge
            residualGraph.insertNode(begin, 0);
            residualGraph.insertNode(end, 0);
            if (0 != directResidual) {
                residualGraph.insertEdgeOneWay(begin, end, new Pair<>(directResidual, directCost));
            }
            if (0 != inverseResidual) {
                residualGraph.insertEdgeOneWay(end, begin, new Pair<>(inverseResidual, -directCost));
            }
        });

        return residualGraph;
    }

    public static Graph<Int, Integer, Triplet<Integer, Integer, Integer>> updateUsingResidualGraph(
            Graph<Int, Integer, Triplet<Integer, Integer, Integer>> graph,
            Graph<Int, Integer, Pair<Integer, Integer>> residualGraph
    ) {
        Graph<Int, Integer, Triplet<Integer, Integer, Integer>> updatedGraph = new Graph<>();

        graph.forEachEdge((begin, end, edgeData) -> {
            int cap, r, flux, cost;
            cap = edgeData.getSecond();
            cost = edgeData.getThird();
            r = (residualGraph.hasEdge(begin, end)) ? residualGraph.edgeData(begin, end).getFirst() : 0;
            flux = cap - r;

            updatedGraph.insertNode(begin, 0);
            updatedGraph.insertNode(end, 0);
            if (flux > 0) {
                updatedGraph.insertEdgeOneWay(begin, end, new Triplet<>(flux, cap, cost));
            } else {
                updatedGraph.insertEdgeOneWay(end, begin, new Triplet<>(-flux, cap, cost));
            }
        });

        return updatedGraph;
    }

    public static Graph<Int, Integer, Pair<Integer, Integer>> getGraphWithoutCosts(Graph<Int, Integer, Triplet<Integer, Integer, Integer>> graph) {
        Graph<Int, Integer, Pair<Integer, Integer>> result = new Graph<>();
        graph.forEachEdge((begin, end, edgeData) -> {
            result.insertNode(begin, 0);
            result.insertNode(end, 0);
            result.insertEdgeOneWay(begin, end, new Pair<>(edgeData.getFirst(), edgeData.getSecond()));
        });
        return result;
    }

    public static Graph<Int, Integer, Triplet<Integer, Integer, Integer>> getGraphWithCosts(Graph<Int, Integer, Triplet<Integer, Integer, Integer>> costs, Graph<Int, Integer, Pair<Integer, Integer>> nocosts) {
        Graph<Int, Integer, Triplet<Integer, Integer, Integer>> result = new Graph<>();
        nocosts.forEachEdge((begin, end, edgeData) -> {
            result.insertNode(begin, 0);
            result.insertNode(end, 0);
            result.insertEdgeOneWay(begin, end, new Triplet<>(edgeData.getFirst(), edgeData.getSecond(), costs.edgeData(begin, end).getThird()));
        });
        return result;
    }
}
