package minflux;

import containers.Int;
import containers.Pair;
import containers.Triplet;
import mst.Graph;

public class MinFlux {
    static String inputFilePath = "minflux.in";

    public static void main(String[] args) {
        Graph<Int, Integer, Triplet<Integer, Integer, Integer>> graph;
        Graph<Int, Integer, Pair<Integer, Integer>> graphForMaxFlux;
        Graph<Int, Integer, Integer> residualGraphForMaxFlux;
        Graph<Int, Integer, Pair<Integer, Integer>> residualGraph;

        // input
        graph = Util.readGraph(inputFilePath);
        System.out.println("Graph:");
        graph.forEachEdge((begin, end, edgeData) -> {
            System.out.println(begin + " - " + end + " " + edgeData);
        });

        // calculate max flux
        graphForMaxFlux = Util.getGraphWithoutCosts(graph);
        System.out.println("Graph without costs:");
        graphForMaxFlux.forEachEdge((begin, end, edgeData) -> {
            System.out.println(begin + " - " + end + " " + edgeData);
        });

        residualGraphForMaxFlux = graphForMaxFlux.maxFlux(maxflux.Util::getResidualGraph, Int.mk(1), Int.mk(4));
        graphForMaxFlux = maxflux.Util.updateUsingResidualGraph(graphForMaxFlux, residualGraphForMaxFlux);

        // update graph
        graph = Util.getGraphWithCosts(graph, graphForMaxFlux);
        System.out.println("After max flux:");
        graph.forEachEdge((begin, end, edgeData) -> {
            System.out.println(begin + " - " + end + " " + edgeData);
        });


        System.out.println("-----------");
        residualGraph = graph.minFlux(Util::getResidualGraph, Int.mk(1), Int.mk(4));
        graph = Util.updateUsingResidualGraph(graph, residualGraph);
        System.out.println("-----------");

        System.out.println("Result:");
        graph.forEachEdge((begin, end, edgeData) -> {
            System.out.println(begin + " - " + end + " " + edgeData);
        });
    }
}